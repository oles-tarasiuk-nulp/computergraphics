const canvas = document.querySelector(".main-canvas");
const ctx = canvas.getContext('2d');

//setting width and height drawing area in canvas
ctx.canvas.width  = canvas.clientWidth;
ctx.canvas.height = canvas.clientHeight;

drawCoordinates();

//changing the size of drawing area according to size of the element
window.addEventListener('resize', function(){
    ctx.canvas.width  = canvas.clientWidth;
    ctx.canvas.height = canvas.clientHeight;
    clearCanvas();
    drawCoordinates();
});


function drawCoordinates(){
    //getting current size of drawing area
    const canvasSize = {
        width: ctx.canvas.width,
        height: ctx.canvas.height
    };

    ctx.lineWidth = 2;
    ctx.font = "20px Arial";
    ctx.textAlign = "left";
    ctx.strokeStyle = 'black';
    ctx.fillStyle = 'black';
    
    ctx.beginPath();
    
    //drawing horizontal and vertical coordinate lines
    ctx.moveTo(0, canvasSize.height / 2);
    ctx.lineTo(canvasSize.width, canvasSize.height / 2);
    ctx.stroke();

    ctx.moveTo(canvasSize.width / 2, 0);
    ctx.lineTo(canvasSize.width / 2, canvasSize.height);
    ctx.stroke();

    //drawing ticks and labels for lines
    for(let i = canvasSize.width / 2; i < canvasSize.width; i += 50){
        ctx.fillText(i - canvasSize.width / 2, i - 20, canvasSize.height / 2 + 20);
        ctx.moveTo(i, canvasSize.height/2 - 3);
        ctx.lineTo(i, canvasSize.height/2 + 3);
        ctx.stroke();
    }
    for(let i = canvasSize.width / 2 - 50; i > 0; i -= 50){
        ctx.fillText(i - canvasSize.width / 2, i - 20, canvasSize.height / 2 + 20);
        ctx.moveTo(i, canvasSize.height/2 - 3);
        ctx.lineTo(i, canvasSize.height/2 + 3);
        ctx.stroke();
    }

    ctx.textAlign = "right";
    for(let i = canvasSize.height / 2 + 50; i < canvasSize.height; i += 50){
        ctx.fillText(canvasSize.height / 2 - i, canvasSize.width/2 - 10, i + 10);
        ctx.moveTo(canvasSize.width/2 - 3, i);
        ctx.lineTo(canvasSize.width/2 + 3, i);
        ctx.stroke();
    }
    for(let i = canvasSize.height / 2 - 50; i > 0; i -= 50){
        ctx.fillText(canvasSize.height / 2 - i, canvasSize.width/2 - 10, i + 10);
        ctx.moveTo(canvasSize.width/2 - 3, i);
        ctx.lineTo(canvasSize.width/2 + 3, i);
        ctx.stroke();
    }

    //drawing arrow at the ends of the lines
    ctx.textAlign = 'left';
    ctx.moveTo(canvasSize.width - 20, canvasSize.height/2 - 8);
    ctx.lineTo(canvasSize.width, canvasSize.height/2);
    ctx.lineTo(canvasSize.width - 20, canvasSize.height/2 + 8);
    ctx.stroke();
    ctx.fillText('x', canvasSize.width - 20, canvasSize.height/2 + 24);

    ctx.moveTo(canvasSize.width/2 - 8, 20);
    ctx.lineTo(canvasSize.width/2, 0);
    ctx.lineTo(canvasSize.width/2 + 8, 20);
    ctx.stroke();
    ctx.fillText('y', canvasSize.width/2 - 24, 20);
}

function drawParallelogram(){
    let isBuild = true;
    const canvasSize = {
        width: ctx.canvas.width,
        height: ctx.canvas.height
    };

    //getting data about the first input dot
    let str = document.getElementById('x-input')
        .value.replace('(', '')
        .replace(')', '')
        .split(',');

    let xPoint = {
        x: null,
        y: null
    }
    //cheking if the data is correct
    try {
        document.getElementById('x-input').previousElementSibling.className = '';
        xPoint.x = Number(str[0].trim());
        xPoint.y = Number(str[1].trim());
        if (isNaN(xPoint.x) || isNaN(xPoint.y) ||
            str[0].trim().length == 0 || str[1].trim().length == 0){
            throw 'error';
        }
        if (Math.abs(xPoint.x) > 10000 || Math.abs(xPoint.y) > 10000){
            throw 'too large number';
        }
    } catch (error) {
        //set red color to label as error indicator
        document.getElementById('x-input').previousElementSibling.className = 'error-animation';
        isBuild = false;
    }

    //getting data about the second input dot
    str = document.getElementById('y-input')
        .value.replace('(', '')
        .replace(')', '')
        .split(',');

    let yPoint = {
        x: null,
        y: null
    }
    //cheking if the data is correct
    try {
        document.getElementById('y-input').previousElementSibling.className = '';
        yPoint.x = Number(str[0].trim());
        yPoint.y = Number(str[1].trim());
        if (isNaN(yPoint.x) || isNaN(yPoint.y) || 
            str[0].trim().length == 0 || str[1].trim().length == 0){
            throw 'error';
        }
        if (Math.abs(yPoint.x) > 10000 || Math.abs(yPoint.y) > 10000){
            throw 'too large number';
        }
    } catch (error) {
        //set red color to label as error indicator
        document.getElementById('y-input').previousElementSibling.className = 'error-animation';
        isBuild = false;
    }

    //getting data about the length of the second side of the parallelogram
    str = document.getElementById('length-input').value;
    let length = null;
    //cheking if the data is correct
    try {
        document.getElementById('length-input').previousElementSibling.className = '';
        length = Number(str.trim());
        if (isNaN(length) || length <= 0){
            throw 'error';
        }
        if (length > 10000){
            throw 'too large length';
        }
    } catch (error) {
        //set red color to label as error indicator
        document.getElementById('length-input').previousElementSibling.className = 'error-animation';
        isBuild = false;
    }

    if (!isBuild){
        return;
    }

    //getting the color of the conture
    str = document.getElementById('color-input').value;

    ctx.strokeStyle = str;
    ctx.beginPath();
    ctx.lineWidth = 3;

    //drawing the parallelogram
    ctx.moveTo(canvasSize.width / 2 + xPoint.x, canvasSize.height / 2 - xPoint.y);
    ctx.lineTo(canvasSize.width / 2 + yPoint.x, canvasSize.height / 2 - yPoint.y);
    ctx.lineTo(canvasSize.width / 2 + yPoint.x + length, canvasSize.height / 2 - yPoint.y);
    ctx.lineTo(canvasSize.width / 2 + xPoint.x + length, canvasSize.height / 2 - xPoint.y);
    ctx.lineTo(canvasSize.width / 2 + xPoint.x, canvasSize.height / 2 - xPoint.y);
    ctx.stroke();
    ctx.fillStyle = getRandomColor();
    ctx.fill();

    //drawing the circle over the parallelogram
    ctx.beginPath();
    ctx.arc(
        canvasSize.width / 2 + (Math.abs(xPoint.x - yPoint.x) + length) / 2 + Math.min(xPoint.x, yPoint.x),
        canvasSize.height / 2 - Math.abs(xPoint.y - yPoint.y) / 2 - Math.min(xPoint.y, yPoint.y),
        Math.sqrt(Math.pow(Math.abs(xPoint.x - yPoint.x) + length, 2) + Math.pow(Math.abs(xPoint.y - yPoint.y), 2)) / 2,
        0,
        Math.PI*2
    );
    ctx.stroke();
}

function clearCanvas(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    const xInput = document.getElementById('x-input');
    xInput.value = null;
    xInput.previousElementSibling.className = '';

    const yInput = document.getElementById('y-input');
    yInput.value = null;
    yInput.previousElementSibling.className = '';

    const lengthInput = document.getElementById('length-input');
    lengthInput.value = null;
    lengthInput.previousElementSibling.className = '';

    document.getElementById('color-input').value = '#000000';

    drawCoordinates();
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}